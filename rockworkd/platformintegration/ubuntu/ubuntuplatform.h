#ifndef UBUNTUPLATFORM_H
#define UBUNTUPLATFORM_H

#include "libpebble/platforminterface.h"
#include "libpebble/enums.h"

#include "notificationmonitor.h"
#include "postalnotifications.h"
#include "musiccontroller.h"

#include <QDBusInterface>
#include <TelepathyQt/AbstractClientObserver>

#include <qdbusactiongroup.h>
#include <qstateaction.h>

class QDBusPendingCallWatcher;
class TelepathyMonitor;
class OrganizerAdapter;
class SyncMonitorClient;

class UbuntuPlatform : public PlatformInterface, public QDBusContext
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.freedesktop.Notifications")


public:
    UbuntuPlatform(QObject *parent = 0);
    ~UbuntuPlatform();

    void sendMusicControlCommand(MusicControlButton controlButton) override;
    MusicMetaData musicMetaData() const override;
    void hangupCall(uint cookie) override;

    QList<CalendarEvent> organizerItems() const override;

    MusicPlayState getMusicPlayState() const override;

    void actionTriggered(const QUuid &uuid, const QString &actToken) const override;
    void removeNotification(const QUuid &uuid) const override;
    bool deviceIsActive() const override;

public slots:
    void onNotification(UbuntuNotifications::Notification *notification);
    void handleClosedNotification(UbuntuNotifications::Notification::CloseReason reason);
    void updateMusicStatus();

private slots:
    void fetchMusicMetadata();
    void mediaPropertiesChanged(const QString &interface, const QVariantMap &changedProps, const QStringList &invalidatedProps);

private:
    MusicMetaData m_musicMetaData;

    TelepathyMonitor *m_telepathyMonitor;
    OrganizerAdapter *m_organizerAdapter;
    mutable QMap<QUuid, UbuntuNotifications::Notification*> m_notifs;
    UbuntuNotifications::NotificationMonitor *m_notificationMonitor;
    PostalNotifications *m_postalNotifications;
    MusicController *m_musicController;
    SyncMonitorClient *m_syncMonitorClient;
    QTimer m_syncTimer;
};

#endif // UBUNTUPLATFORM_H
