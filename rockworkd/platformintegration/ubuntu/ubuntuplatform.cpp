#include "ubuntuplatform.h"

#include "callchannelobserver.h"
#include "organizeradapter.h"
#include "syncmonitorclient.h"
#include "notificationmonitor.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDebug>

// qmenumodel
#include "dbus-enums.h"
#include "liblomiri-url-dispatcher/lomiri-url-dispatcher.h"

UbuntuPlatform::UbuntuPlatform(QObject *parent):
    PlatformInterface(parent)
{
    // Notifications
    m_notificationMonitor = new UbuntuNotifications::NotificationMonitor(this);
    m_postalNotifications = new PostalNotifications();
    connect(m_notificationMonitor, &UbuntuNotifications::NotificationMonitor::notification, this, &UbuntuPlatform::onNotification);

    // Music
    m_musicController = new MusicController(this);
    connect(m_musicController, SIGNAL(metadataChanged()), SLOT(fetchMusicMetadata()));
    connect(m_musicController, SIGNAL(statusChanged()), SLOT(updateMusicStatus()));
    connect(m_musicController, SIGNAL(positionChanged()), SLOT(updateMusicStatus()));

    // Calls
    m_telepathyMonitor = new TelepathyMonitor(this);
    connect(m_telepathyMonitor, &TelepathyMonitor::incomingCall, this, &UbuntuPlatform::incomingCall);
    connect(m_telepathyMonitor, &TelepathyMonitor::callStarted, this, &UbuntuPlatform::callStarted);
    connect(m_telepathyMonitor, &TelepathyMonitor::callEnded, this, &UbuntuPlatform::callEnded);

    // Organizer
    m_organizerAdapter = new OrganizerAdapter(this);
    m_organizerAdapter->refresh();
    connect(m_organizerAdapter, &OrganizerAdapter::itemsChanged, this, &UbuntuPlatform::organizerItemsChanged);
    m_syncMonitorClient = new SyncMonitorClient(this);
    connect(m_syncMonitorClient, &SyncMonitorClient::stateChanged, [this]() { if (m_syncMonitorClient->state() == "idle") m_organizerAdapter->refresh();});
    m_syncTimer.start(1000 * 60 * 60);
    connect(&m_syncTimer, &QTimer::timeout, [this]() { m_syncMonitorClient->sync({"calendar"});});
    m_syncMonitorClient->sync({"calendar"});
}

UbuntuPlatform::~UbuntuPlatform()
{
    delete m_notificationMonitor;
    delete m_postalNotifications;
    delete m_musicController;
    delete m_telepathyMonitor;
    delete m_organizerAdapter;
    delete m_syncMonitorClient;
}

void UbuntuPlatform::onNotification(UbuntuNotifications::Notification *notification) {

    qDebug() << "Got new notification for watch: " << notification->appName() << notification->summary();

    QString owner = notification->sender();
    Notification n(owner);
    if (owner.contains("twitter")) {
        n.setType(Notification::NotificationTypeTwitter);
        n.setSourceId("Twitter");
    } else if (owner.contains("dekko")) {
        if (notification->sender().toLower().contains("gmail")) {
            n.setType(Notification::NotificationTypeGMail);
            n.setSender("GMail");
        }
        else {
            n.setType(Notification::NotificationTypeEmail);
            n.setSender(notification->sender());
            n.setSourceId(notification->sender());
        }
    } else if (owner == "Telephony Service Indicator") { // Find a better way to handle this :P
        n.setType(Notification::NotificationTypeSMS);
        n.setSender("SMS");
        n.setSourceId("SMS");
    } else if (owner.contains("teleports")) {
        n.setType(Notification::NotificationTypeTelegram);
    } else {
        n.setType(Notification::NotificationTypeGeneric);
        n.setSender(notification->sender());
    }
    n.setSubject(notification->summary());
    n.setBody(notification->body());
    n.setIcon(notification->icon());

    n.setAppName(notification->appName());

    foreach (const QString &action, notification->actions()) {
        if (action.contains(QRegExp("^[a-z]*://"))) {
            n.setActToken(action);
            break;
        }
    }
    connect(notification, &UbuntuNotifications::Notification::closed,
            this, &UbuntuPlatform::handleClosedNotification);
    qDebug() << "have act token" << n.actToken();
    qDebug() << "mapping " << &n << " to " << notification;
    m_notifs.insert(n.uuid(), notification);
    emit notificationReceived(n);
}

void UbuntuPlatform::handleClosedNotification(UbuntuNotifications::Notification::CloseReason reason) {
    UbuntuNotifications::Notification *n = static_cast<UbuntuNotifications::Notification*>(sender());
    qDebug() << "Notification closed:" << n->id() << "Reason: " << reason;
    disconnect(n, 0, this, 0);
    QMap<QUuid, UbuntuNotifications::Notification*>::iterator it = m_notifs.begin();
    while (it != m_notifs.end()) {
        if (it.value() && it.value()->id() == n->id()) {
            qDebug() << "Found notification to remove " << it.key();
            emit notificationRemoved(it.key());
            m_notifs.erase(it);
            return;
        }
        it++;
    }
    qDebug() << "Notification not found";
}

void UbuntuPlatform::sendMusicControlCommand(MusicControlButton controlButton)
{
    switch (controlButton) {
    case MusicControlPlayPause:
        m_musicController->playPause();
        break;
    case MusicControlPause:
        m_musicController->pause();
        break;
    case MusicControlPlay:
        m_musicController->play();
        break;
    case MusicControlPreviousTrack:
        m_musicController->previous();
        break;
    case MusicControlNextTrack:
        m_musicController->next();
        break;
    case MusicControlVolumeUp:
        m_musicController->volumeUp();
        break;
    case MusicControlVolumeDown:
        m_musicController->volumeDown();
        break;
    default:
        ;
    }
}

MusicMetaData UbuntuPlatform::musicMetaData() const
{
    return m_musicMetaData;
}

void UbuntuPlatform::hangupCall(uint cookie)
{
    m_telepathyMonitor->hangupCall(cookie);
}

QList<CalendarEvent> UbuntuPlatform::organizerItems() const
{
    return m_organizerAdapter->items();
}

void UbuntuPlatform::actionTriggered(const QUuid &uuid, const QString &actToken) const
{
    qDebug() << "Triggering notification " << uuid << " action " << actToken;
    UbuntuNotifications::Notification *notif = m_notifs.value(uuid);
    if (notif) {
        removeNotification(uuid);
    }
    else
        qDebug() << "Not found";

    lomiri_url_dispatch_send(actToken.toStdString().c_str(), [] (const gchar *, gboolean, gpointer) {}, nullptr);
}

void UbuntuPlatform::removeNotification(const QUuid &uuid) const
{
    qDebug() << "Removing notification " << uuid;
    UbuntuNotifications::Notification *notif = m_notifs.value(uuid);
    if (notif) {
        m_postalNotifications->closeNotification(notif->sender());
        m_notifs.remove(uuid);
    }
    else
        qDebug() << "Not found";
}

void UbuntuPlatform::fetchMusicMetadata()
{
    m_musicMetaData.artist = m_musicController->artist();
    m_musicMetaData.album = m_musicController->album();
    m_musicMetaData.title = m_musicController->title();
    m_musicMetaData.duration = m_musicController->duration();
    qDebug() << "New track " << m_musicMetaData.title << ". Length: " << m_musicMetaData.duration;
    emit musicMetadataChanged(m_musicMetaData);
    updateMusicStatus();
}

void UbuntuPlatform::updateMusicStatus() {
    MusicPlayState playState = getMusicPlayState();
    emit musicPlayStateChanged(playState);
}

MusicPlayState UbuntuPlatform::getMusicPlayState() const {
    MusicPlayState playState;
    switch (m_musicController->status()) {
        case MusicController::Status::StatusNoPlayer:
            playState.state = MusicPlayState::StateUnknown;
        break;
        case MusicController::Status::StatusStopped:
            playState.state = MusicPlayState::StatePaused;
        break;
        case MusicController::Status::StatusPaused:
            playState.state = MusicPlayState::StatePaused;
        break;
        case MusicController::Status::StatusPlaying:
            playState.state = MusicPlayState::StatePlaying;
        break;
        default:
            playState.state = MusicPlayState::StateUnknown;
    }
    switch (m_musicController->repeat()) {
        case MusicController::RepeatStatus::RepeatNone:
            playState.repeat = MusicPlayState::RepeatOff;
        break;
        case MusicController::RepeatStatus::RepeatTrack:
            playState.repeat = MusicPlayState::RepeatOne;
        break;
        case MusicController::RepeatStatus::RepeatPlaylist:
            playState.repeat = MusicPlayState::RepeatAll;
        break;
        default:
            playState.repeat = MusicPlayState::RepeatUnknown;
    }

    playState.trackPosition = m_musicController->position()/1000;
    if (m_musicController->shuffle())
        playState.shuffle = MusicPlayState::ShuffleOn;
    else
        playState.shuffle = MusicPlayState::ShuffleOff;

    return playState;
}

void UbuntuPlatform::mediaPropertiesChanged(const QString &interface, const QVariantMap &changedProps, const QStringList &invalidatedProps)
{
    Q_UNUSED(interface)
    Q_UNUSED(changedProps)
    Q_UNUSED(invalidatedProps)
    fetchMusicMetadata();
}

bool UbuntuPlatform::deviceIsActive() const
{
    return true; // HACK: implement using repowerd later
}
