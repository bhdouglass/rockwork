#include "postalnotifications.h"

PostalNotifications::PostalNotifications():
  m_bus(QDBusConnection::sessionBus())
{
}

PostalNotifications::~PostalNotifications()
{
}

void PostalNotifications::closeNotification(QString appId)
{
    QString pkgName = appId.split("_").at(0);
    pkgName = pkgName.replace(".", "_2e").replace("-", "_2d");

    QString path = "/" + pkgName;

    QDBusInterface iface(DBUS_SERVICE, DBUS_PATH + path, DBUS_IFACE, QDBusConnection::sessionBus());
    QDBusReply<QStringList> reply = iface.call("ListPersistent", appId);
    QDBusMessage message = QDBusMessage::createMethodCall(DBUS_SERVICE, DBUS_PATH + path, DBUS_IFACE, "ClearPersistent");
    message << appId;
    for (int i = 0; i < reply.value().size(); i++) {
        message << reply.value().at(i);
    }
    m_bus.send(message);
    m_bus.call(message);
}
