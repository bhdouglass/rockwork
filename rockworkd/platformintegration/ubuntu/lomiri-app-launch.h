#ifndef LOMIRI_APP_LAUNCH_H
#define LOMIRI_APP_LAUNCH_H

#include <lomiri-app-launch/application.h>
#include <lomiri-app-launch/registry.h>

#include <QStringList>
#include <memory>

class LomiriAppLaunch {

public:
    LomiriAppLaunch();
    ~LomiriAppLaunch();

    QString name() const;
    QString icon() const;

    bool populate(const QString &id);

private:
    QString m_name;
    QString m_icon;
};

#endif // LOMIRI_APP_LAUNCH_H
