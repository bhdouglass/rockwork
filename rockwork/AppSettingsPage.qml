import QtQuick 2.4
import Ubuntu.Web 0.2
import Ubuntu.Components 1.3
import Morph.Web 0.1
import QtWebEngine 1.7
import "js-dialogs" as Dialogs

Page {
    id: settings

    property string uuid;
    property string url;
    property var pebble;
    property var contextMenuRequest: null

    title: i18n.tr("App Settings")

    WebContext {
        id: webcontext
        userAgent: "Mozilla/5.0 (Linux; Android 8.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.151 Mobile Safari/537.36 Ubuntu Touch (RockWork)"
        offTheRecord: false
    }

    WebView {
        id: webview
        anchors {
            fill: parent
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height
        zoomFactor: 2.5

        context: webcontext
        url: settings.url

        // Copied from morph: https://github.com/ubports/morph-browser/blob/8d95c2b123c71a9368617f429643c8182685235d/src/app/WebViewImpl.qml
        onJavaScriptDialogRequested: function(request) {
            switch (request.type)
            {
                case JavaScriptDialogRequest.DialogTypeAlert:
                    request.accepted = true;
                    var alertDialog = PopupUtils.open(Qt.resolvedUrl('js-dialogs/AlertDialog.qml'));
                    alertDialog.message = request.message;
                    alertDialog.accept.connect(request.dialogAccept);
                    break;

                case JavaScriptDialogRequest.DialogTypeConfirm:
                    request.accepted = true;
                    var confirmDialog = PopupUtils.open(Qt.resolvedUrl('js-dialogs/ConfirmDialog.qml'));
                    confirmDialog.message = request.message;
                    confirmDialog.accept.connect(request.dialogAccept);
                    confirmDialog.reject.connect(request.dialogReject);
                    break;

                case JavaScriptDialogRequest.DialogTypePrompt:
                    request.accepted = true;
                    var promptDialog = PopupUtils.open(Qt.resolvedUrl('js-dialogs/PromptDialog.qml'));
                    promptDialog.message = request.message;
                    promptDialog.defaultValue = request.defaultText;
                    promptDialog.accept.connect(request.dialogAccept);
                    promptDialog.reject.connect(request.dialogReject);
                    break;

                // did not work with JavaScriptDialogRequest.DialogTypeUnload (the default dialog was shown)
                //case JavaScriptDialogRequest.DialogTypeUnload:
                case 3:
                    request.accepted = true;
                    var beforeUnloadDialog = PopupUtils.open(Qt.resolvedUrl('js-dialogs/BeforeUnloadDialog.qml'));
                    beforeUnloadDialog.message = request.message;
                    beforeUnloadDialog.accept.connect(request.dialogAccept);
                    beforeUnloadDialog.reject.connect(request.dialogReject);
                    break;
            }
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            if (url.substring(0, 16) == 'pebblejs://close') {
                pebble.configurationClosed(settings.uuid, url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
                pageStack.pop();
            }
        }

        onContextMenuRequested: function(request) {
            contextMenuRequest = request;
            request.accepted = true;
            contextMenu.visible = true;
            console.log(JSON.stringify(request));
        }
    }

    // Adapted from https://github.com/ubports/morph-browser/blob/fca4c67ac56dbdfd5748385337492089957575be/src/app/WebViewImpl.qml#L251
    UbuntuShape {
        id: contextMenu
        visible: false

        // TODO make this close to where the context menu was requested
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 3

        aspect: UbuntuShape.DropShadow
        backgroundColor: theme.palette.normal.background
        readonly property int padding: units.gu(1)
        width: actionRow.width + padding * 2
        height: childrenRect.height + padding * 2

        Row {
            id: actionRow
            x: parent.padding
            y: parent.padding
            height: units.gu(6)

            Repeater {
                model: actions.children
                AbstractButton {
                    anchors {
                        top: actionRow.top
                        bottom: actionRow.bottom
                    }
                    width: Math.max(units.gu(4), implicitWidth) + units.gu(1)
                    action: modelData
                    styleName: "ToolbarButtonStyle"
                    activeFocusOnPress: false
                }
            }
        }

        ActionList {
            id: actions

            Action {
                name: "cut"
                text: i18n.tr("Cut")
                iconName: "edit-cut"
                enabled: contextMenuRequest && contextMenuRequest.isContentEditable
                visible: enabled
                onTriggered: {
                    contextMenu.visible = false;
                    webview.triggerWebAction(WebEngineView.Cut);
                }
            }

            Action {
                name: "copy"
                text: i18n.tr("Copy")
                iconName: "edit-copy"
                enabled: contextMenuRequest
                visible: enabled
                onTriggered: {
                    contextMenu.visible = false;
                    webview.triggerWebAction(WebEngineView.Copy)
                }
            }

            Action {
                name: "paste"
                text: i18n.tr("Paste")
                iconName: "edit-paste"
                enabled: contextMenuRequest && contextMenuRequest.isContentEditable && (contextMenuRequest.editFlags & ContextMenuRequest.CanPaste)
                visible: enabled
                onTriggered: {
                    contextMenu.visible = false;
                    webview.triggerWebAction(WebEngineView.Paste);
                }
            }

            // TODO remove this and hide when the user taps a different area
            Action {
                name: "close"
                text: i18n.tr("Close")
                iconName: "close"
                enabled: true
                visible: enabled
                onTriggered: {
                    contextMenu.visible = false;
                }
            }
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    /*Connections {
        target: UriHandler
        onOpened: {
            if (uris && uris[0] && uris[0].length) {
                var url = uris[0];

                if (url.substring(0, 16) == 'pebblejs://close') {
                    pebble.configurationClosed(settings.uuid, url);
                    pageStack.pop();
                }
            }
        }
    }*/
}
